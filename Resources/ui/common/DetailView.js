var osname = Ti.Platform.osname;
var fb = require('facebook');
var aztagDaily = require('services/AztagDaily');

function DetailView(data, pushNotificationWindow) {
	var theSharedData = (data.title) + "\n" + L('breaking_news');

	var self = Ti.UI.createWindow({
		backgroundColor : 'white',
		translucent : false,
		layout : 'vertical',
		barColor : "#fdc616",
		navTintColor : "#FFF"
	});
	if (osname == 'android') {
		self.navBarHidden = true;
		self.fullscreen = false;
		self.exitOnClose = (pushNotificationWindow) ? true : false;
		self.orientationModes = [Ti.UI.PORTRAIT];
	} else {
		/**
		 * this will make the back button in ios i18n
		 */
		self.backButtonTitle = L("back");
	}

	var infoBar = Ti.UI.createView({
		width : 'auto',
		height : 'auto',
		backgroundColor : '#fdc616',
		height : '44dp',
	});

	var title = Ti.UI.createLabel({
		ellipsize : true,
		wordWrap : false,
		width : '75%',
		// height: '85%',
		color : 'black',
		left : '10dp',
		top : '5dp',
		font : {
			fontSize : '18dp',
			fontWeight : 'bold'
		}
	});
	title.text = (data.pushNotificationNews) ? L('breaking_news') : ((data.title) ? (data.title) : '');

	var date = Ti.UI.createLabel({
		text : data.date || (new Date().toDateString()),
		color : 'black',
		top : '5dp',
		font : {
			fontSize : '12dp',
			fontWeight : 'bold'
		}
	});
	var time = Ti.UI.createLabel({
		text : data.time || (new Date().toTimeString()),
		color : 'black',
		top : '20dp',
		font : {
			fontSize : '12dp',
			fontWeight : 'bold'
		}
	});
	infoBar.add(title);
	if (osname === 'android') {
		self.add(infoBar);
	}

	var imageHolder = Ti.UI.createView({
		width : Ti.UI.FILL,
		height : '45%',
		backgroundColor : '#000000'
	});

	var image = Ti.UI.createImageView({
		width : Ti.UI.FILL,
		height : Ti.UI.FILL
	});
	if (osname === 'android') {
		image.image = (data.image) ? data.image : '/images/detailViewLogo.png';
	} else {
		image.image = (data.image) ? data.image : 'images/detailViewLogo.png';
	}
	imageHolder.add(image);
	self.add(imageHolder);

	var toolbar = Ti.UI.createView({
		width : 'auto',
		height : 'auto',
		backgroundColor : '#ececec',
		height : '44dp',

	});

	var decreaseBtn = Ti.UI.createButton({
		height : '40dp',
		width : '40dp',
		left : '5dp'
	});
	decreaseBtn.backgroundImage = (osname === 'android') ? '/images/ZoomOut-black.png' : 'images/ZoomOut-black.png';

	decreaseBtn.addEventListener('click', function() {
		var newFontSize = lbl.getFont().fontSize - 3;
		lbl.setFont({
			fontSize : newFontSize
		});
	});
	var increaseBtn = Ti.UI.createButton({
		height : '40dp',
		width : '40dp',
		left : '55dp'
	});
	increaseBtn.backgroundImage = (osname === 'android') ? '/images/ZoomIn-black.png' : 'images/ZoomIn-black.png';
	increaseBtn.addEventListener('click', function() {
		var newFontSize = lbl.getFont().fontSize + 3;
		lbl.setFont({
			fontSize : newFontSize
		});
	});

	var facebookBtn = Ti.UI.createButton({
		height : '40dp',
		width : '40dp',
		right : '15dp'
	});
	facebookBtn.backgroundImage = 'images/facebook-icon.png';
	//this is ios only
	facebookBtn.addEventListener('click', function() {
		fb.appid = aztagDaily.fbAppId;
		fb.permissions = ['publish_stream'];
		// Permissions your app needs
		fb.forceDialogAuth = false;
		fb.addEventListener('login', function(e) {
			if (e.error) {
				alert(L('something_wrong'));
			}
		});

		if (!fb.loggedIn) {
			fb.authorize();
		}
		var shareData = {
			link : data.link || '',
			name : data.title || '',
			message : data.title || '',
			picture : data.image || '',
			description : data.description || ''
		};
		fb.dialog("feed", shareData, function(e) {
			if (e.error) {
				alert(L('something_wrong'));
			}
		});
	});

	/**
	 * the follwing share button is exclusive to android as it uses
	 * android intents to share to installed applications.
	 */
	var intentShareBtn = Ti.UI.createButton({
		backgroundImage : '/images/share-black.png',
		height : '40dp',
		width : '40dp',
		right : '15dp'
	});
	intentShareBtn.addEventListener('click', function() {
		var intent = Ti.Android.createIntent({
			action : Ti.Android.ACTION_SEND,
			type : "text/plain"
		});
		intent.putExtra(Ti.Android.EXTRA_TEXT, theSharedData);
		intent.addCategory(Ti.Android.CATEGORY_DEFAULT);
		var chooser = Ti.Android.createIntentChooser(intent, L("share"));
		Ti.Android.currentActivity.startActivity(chooser);
	});

	toolbar.add(decreaseBtn);
	toolbar.add(increaseBtn);
	toolbar.add(date);
	toolbar.add(time);

	if (osname === 'android') {
		toolbar.add(intentShareBtn);
	} else {
		var Social = require('dk.napp.social');
		
		var activityBtn = Ti.UI.createButton();
		activityBtn.width = '40dp';
		activityBtn.height = '40dp';
		activityBtn.backgroundImage = 'images/ios-share-icon.png';

		if (Social.isFacebookSupported()) {
			activityBtn.right = '15dp';
		} else {
			activityBtn.right = '55dp';
			toolbar.add(facebookBtn);
		}

		if (Ti.Platform.osname === 'ipad') {
			/**
			 * this is specific for ipad as the share button should be used as a popover
			 */
			if (Social.isActivityViewSupported()) {
				activityBtn.addEventListener('click', function() {
					Social.activityPopover({
						text : theSharedData,
						emailIsHTML : false,
						view : activityBtn
					});
				});
				toolbar.add(activityBtn);
			}
		} else {
			/**
			 * this is specific to ios as the share button should be an activity view
			 */
			if (Social.isActivityViewSupported()) {//min iOS6 required
				activityBtn.addEventListener('click', function() {
					Social.activityView({
						text : theSharedData,
						emailIsHTML : false
					});
				});
				toolbar.add(activityBtn);
			}
		}

	}
	self.add(toolbar);

	var scrollView = Ti.UI.createScrollView({
		layout : 'vertical',
		scrollType : 'vertical',
		disableBounce : true,
		contentWidth : 'auto',
		contentHeight : 'auto',
		showVerticalScrollIndicator : false,
		showHorizontalScrollIndicator : false,
		height : 'auto',
		width : '95%'
	});

	var lbl = Ti.UI.createLabel({
		height : 'auto',
		width : 'auto',
		wordWrap : true,
		color : '#000',
		font : {
			fontSize : 20
		}
	});
	var theText = (data.pushNotificationNews) ? ((data.title) ? (data.title) : '') : ((data.description) ? (data.description) : '');
	lbl.text = theText;
	scrollView.add(lbl);

	self.add(scrollView);

	if (osname === 'android') {
		self.addEventListener('open', function() {
			//hides the action bar
			self.activity.getActionBar().hide();
		});
	}

	return self;
};

module.exports = DetailView;
