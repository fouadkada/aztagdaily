function createTableRows(dataObj) {
	var result = [];

	for (var i = 0, newsRows = dataObj.length; i < newsRows; i++) {
		var currentItem = dataObj[i];
		var row = Ti.UI.createTableViewRow({
			className : 'row',
			height : '50dp',
			width : 'auto',
			currentItem : currentItem,
			backgroundColor : '#e9e4e1'
		});

		var title = Ti.UI.createLabel({
			text : currentItem.title,
			top : '1dp',
			left : '10dp',
			height : '45dp',
			width : '88%',
			textAlign : 'left',
			ellipsize : false,
			wordWrap : true,
			color : 'black',
			font : {
				fontSize : '12dp'
			}
		});

		var time = Ti.UI.createLabel({
			text : currentItem.time,
			font : {
				fontSize : '10dp',
				fontWeight : 'bold'
			},
			color : '#5f686e',
			right : '10dp',
			bottom : '1dp',
			color : 'black'
		});

		row.add(title);
		row.add(time);
		/**
		 * first item or current item date different that previous item date
		 *
		 */
		if (i == 0 || (i != 0 && (currentItem.date != dataObj[i - 1].date))) {
			var tableViewSectionHeaderView = Ti.UI.createView({
				backgroundColor : 'black',
				height : '20dp'
			});
			var dateHeaderView = Ti.UI.createLabel({
				color : 'white',
				font : {
					fontSize : '13dp'
				},
				left : '10dp',
				text : currentItem.date
			});
			tableViewSectionHeaderView.add(dateHeaderView);
			var section = Ti.UI.createTableViewSection({
				headerView : tableViewSectionHeaderView
			});
			section.add(row);
			result.push(section);
		} else {
			result[result.length - 1].add(row);
		}
	}
	return result;
};

function createEmptyTableRow() {
	var rows = [];
	var row = Ti.UI.createTableViewRow({
		title : L('no_news'),
		color : 'black'
	});
	rows.push(row);
	return rows;
}

function processRSS(_args) {
	if (Object.prototype.toString.call(_args) === '[object Array]') {
		var rows = [];
		var data = [];
		for (var i = 0, arrayLength = _args.length; i < arrayLength; i++) {
			var currentIndex = _args[i];
			try {
				currentIndex = Ti.XML.parseString(currentIndex);
			} catch(e) {
				Ti.API.debug('something went wrong while parsing xml: ' + ((e.error) || JSON.stringify(e)));
				return createEmptyTableRow();
			}
			try {
				var items = currentIndex.documentElement.getElementsByTagName("item");
				for (var j = 0; j < items.length; j++) {
					var item = items.item(j);
					var image;
					try {
						image = item.getElementsByTagNameNS('http://search.yahoo.com/mrss/', 'content').item(0).getAttribute('url');
					} catch (e) {
						image = '';
					}
					var fullDate = new Date(item.getElementsByTagName('pubDate').item(0).textContent);
					var offset = fullDate.getTimezoneOffset() * -1;
					// offset = offset * -1;
					fullDate.setMinutes(fullDate.getMinutes() + offset);

					var year = fullDate.getUTCFullYear();
					var month = ((fullDate.getUTCMonth() + 1) < 10) ? ("0" + (fullDate.getUTCMonth() + 1)) : ((fullDate.getUTCMonth() + 1));
					var day = (fullDate.getUTCDate() < 10) ? ("0" + fullDate.getUTCDate()) : (fullDate.getUTCDate());
					var dmy = day + '/' + month + '/' + year;

					var hours = (fullDate.getUTCHours() < 10) ? ("0" + fullDate.getUTCHours()) : (fullDate.getUTCHours());
					var minutes = (fullDate.getUTCMinutes() < 10) ? ("0" + fullDate.getUTCMinutes()) : (fullDate.getUTCMinutes());
					var hm = hours + ':' + minutes;

					var newsObj = {
						title : item.getElementsByTagName('title').item(0).textContent,
						link : item.getElementsByTagName('link').item(0).textContent,
						date : dmy,
						time : hm,
						pubDate : fullDate,
						description : item.getElementsByTagName('aztag').item(0).textContent || '',
						image : image
					};
					newsObj.pushNotificationNews = (newsObj.description === '') ? true : false;

					data.push(newsObj);
				}
			} catch(e) {
				Ti.API.debug('something went wrong while creating news object: ' + ((e.error) || JSON.stringify(e)));
				return createEmptyTableRow();
			}
		}
		if (data.length) {
			data.sort(function(a, b) {
				var aDate = new Date(a.pubDate);
				var bDate = new Date(b.pubDate);
				if (aDate < bDate) {
					return 1;
				} else if (aDate > bDate) {
					return -1;
				}
				return 0;
			});

			var temp = createTableRows(data);
			for (var j = 0; j < temp.length; j++) {
				var current = temp[j];
				rows.push(current);
			}

		}
		if (rows.length) {
			return rows;
		} else {
			return createEmptyTableRow();
		}
	}
}

module.exports = {
	success : function(response) {
		return processRSS(response);
	},
	error : function() {
		return createEmptyTableRow();
	},
	offline : function() {
		alert(L('error_fetching_api'));
	}
};
