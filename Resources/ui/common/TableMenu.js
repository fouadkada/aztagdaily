function TableMenu(parentWindow) {
	var Cloud = require('ti.cloud');
	var osname = Ti.Platform.osname;
	var deviceTokenKey = 'deviceToken';
	var pnEnabledKey = 'pnEnabled';
	var firstBootKey = 'firstBoot';
	var pnUsername = 'pushnotificationuser';
	var pnPassword = 'pushp@ssw0rd';
	// var pnUsername = 'testpushnotificationuser';
	// var pnPassword = 'testpushnotificationuser';
	var pnChannel = 'aztag_news';
	var DetailView = require('ui/common/DetailView');
	var switchButton = Ti.UI.createSwitch();

	function enablePushNotifications() {
		Ti.Analytics.featureEvent('chah.enablePN');
		Cloud.Users.login({
			login : pnUsername,
			password : pnPassword
		}, function(e) {
			if (e.success) {
				Ti.Analytics.featureEvent('chah.pnEnableLoginSuccess');
				Cloud.PushNotifications.subscribe({
					channel : pnChannel,
					device_token : Ti.App.Properties.getString(deviceTokenKey, ''),
					type : osname === 'android' ? 'android' : 'ios'
				}, function(event) {
					if (event.success) {
						Ti.Analytics.featureEvent('chah.pnSubscribeSuccess');
						Ti.API.debug('successfully registered for aztag_news channel');
						Ti.App.Properties.setBool(pnEnabledKey, true);
					} else {
						Ti.Analytics.featureEvent('chah.pnSubscribeFailure');
						Ti.API.debug('Error registering for push notifications: ' + ((event.error && event.message) || JSON.stringify(event)));
						Ti.App.Properties.setBool(pnEnabledKey, false);
						switchButton.value = false;
					}
				});
			} else {
				Ti.Analytics.featureEvent('chah.pnSubscribeLoginFailure');
				Ti.API.debug('Error logging in for push notifications: ' + ((e.error && e.message) || JSON.stringify(e)));
				Ti.App.Properties.setBool(pnEnabledKey, false);
				switchButton.value = false;
			}
		});
	};

	function disablePushNotifications() {
		Ti.Analytics.featureEvent('chah.disablePN');
		Cloud.Users.login({
			login : pnUsername,
			password : pnPassword
		}, function(e) {
			if (e.success) {
				Ti.Analytics.featureEvent('chah.pnDisableLoginSuccess');
				Cloud.PushNotifications.unsubscribe({
					channel : pnChannel,
					device_token : Ti.App.Properties.getString(deviceTokenKey, ''),
					type : osname === 'android' ? 'android' : 'ios'
				}, function(e) {
					if (e.success) {
						Ti.Analytics.featureEvent('chah.pnUnsubscribeSuccess');
						Ti.API.debug('Successfully unsubscribed from push notifications');
						Ti.App.Properties.setBool(pnEnabledKey, false);
					} else {
						Ti.Analytics.featureEvent('chah.pnUnsubscribeFailure');
						Ti.API.debug('Error unsubscribing from push notifications' + ((e.error && e.message) || JSON.stringify(e)));
						Ti.App.Properties.setBool(pnEnabledKey, true);
						switchButton.value = true;
					}
				});
			} else {
				Ti.Analytics.featureEvent('chah.pnUnsubscribeLoginFailure');
				Ti.API.debug('Error logging in for push notifications: ' + ((e.error && e.message) || JSON.stringify(e)));
				Ti.App.Properties.setBool(pnEnabledKey, true);
				switchButton.value = true;
			}
		});
	};

	function handlePushNotificationMessages(payload) {
		Ti.Analytics.featureEvent('chah.handlePN');
		var newsObj = {
			title : payload.postTitle,
			link : payload.postUrl,
			date : payload.postDate,
			time : payload.postTime,
			pubDate : '',
			description : '',
			image : ''
		};
		newsObj.pushNotificationNews = true;
		var detailView = new DetailView(newsObj, true);
		if (parentWindow) {
			var currentBadge = Ti.UI.iPhone.getAppBadge();
			if (currentBadge > 0) {
				Ti.UI.iPhone.setAppBadge(0);
			}
			if (parentWindow.isLeftWindowOpen()) {
				parentWindow.toggleLeftWindow();
			}
			parentWindow.getCenterWindow().openWindow(detailView, {
				//this is mainly for ios devices
				animated : true
			});
		} else {
			detailView.open();
		}
	};

	function retrieveDeviceToken(cb) {
		function successCb(e) {
			Ti.API.debug('successfully registered for push notifications with device token: ' + e.deviceToken);
			Ti.App.Properties.setString(deviceTokenKey, e.deviceToken);
			cb(null, e.deviceToken);
		};

		function errorCb(e) {
			Ti.API.debug('Failed to register for push notifications! ' + e.error);
			Ti.App.Properties.setString(deviceTokenKey, '');
			cb(e.error, null);
		}

		if (osname === 'android') {
			var CloudPush = require('ti.cloudpush');
			CloudPush.retrieveDeviceToken({
				success : successCb,
				error : errorCb
			});
			// Process incoming push notifications
			CloudPush.addEventListener('callback', function(evt) {
				var payload = JSON.parse(evt.payload);
				handlePushNotificationMessages(payload);
			});
		} else {
			// Check if the device is running iOS 8 or later
			if (Ti.Platform.name == "iPhone OS" && parseInt(Ti.Platform.version.split(".")[0]) >= 8) {
				function registerForPush() {
					Ti.Network.registerForPushNotifications({
						success : successCb,
						error : errorCb,
						callback : function(e) {
							var payload = e.data;
							handlePushNotificationMessages(payload);
						}
					});
					// Remove event listener once registered for push notifications
					Ti.App.iOS.removeEventListener('usernotificationsettings', registerForPush);
				};

				// Wait for user settings to be registered before registering for push notifications
				Ti.App.iOS.addEventListener('usernotificationsettings', registerForPush);

				// Register notification types to use
				Ti.App.iOS.registerUserNotificationSettings({
					types : [Ti.App.iOS.USER_NOTIFICATION_TYPE_ALERT, Ti.App.iOS.USER_NOTIFICATION_TYPE_SOUND, Ti.App.iOS.USER_NOTIFICATION_TYPE_BADGE]
				});

			} else {
				// For iOS 7 and earlier
				Ti.Network.registerForPushNotifications({
					// Specifies which notifications to receive
					types : [Ti.Network.NOTIFICATION_TYPE_BADGE, Ti.Network.NOTIFICATION_TYPE_ALERT, Ti.Network.NOTIFICATION_TYPE_SOUND],
					success : successCb,
					error : errorCb,
					callback : function(e) {
						var payload = e.data;
						handlePushNotificationMessages(payload);
					}
				});
			}
		}
	};

	function handleSwitchButtonChanges(e) {
		var tokenKey = Ti.App.Properties.getString(deviceTokenKey, '');
		if (!tokenKey) {
			retrieveDeviceToken(function(err, deviceToken) {
				if (!err && deviceToken) {
					(e.value) ? enablePushNotifications() : disablePushNotifications();
				}
			});
		} else {
			(e.value) ? enablePushNotifications() : disablePushNotifications();
		}
	};

	function createSwitchButton(text) {
		switchButton.value = false;
		switchButton.itemId = 'swicthButton';
		switchButton.right = '6dp';

		var deviceToken = Ti.App.Properties.getString(deviceTokenKey, '');
		var pushNotificationsEnabled = Ti.App.Properties.getBool(pnEnabledKey, false);
		var firstBoot = Ti.App.Properties.getBool(firstBootKey, true);
		if (firstBoot) {
			retrieveDeviceToken(function(err, token) {
				if (!err && token) {
					switchButton.value = true;
					firstBoot = false;
					Ti.App.Properties.setBool(firstBootKey, false);
					handleSwitchButtonChanges({
						value : true
					});
				} else {
					switchButton.value = false;
					Ti.App.Properties.setBool(firstBootKey, true);
					Ti.App.Properties.setString(deviceTokenKey, '');
					Ti.App.Properties.setBool(pnEnabledKey, false);
					firstBoot = true;
				}
			});
		} else {
			retrieveDeviceToken(function(err, token) {
				if (!err && token) {
					switchButton.value = (token && pushNotificationsEnabled) ? true : false;
					handleSwitchButtonChanges({
						value : switchButton.value
					});
				} else {
					switchButton.value = false;
					Ti.App.Properties.setString(deviceTokenKey, '');
					Ti.App.Properties.setBool(pnEnabledKey, false);
					switchButton.value = false;
					handleSwitchButtonChanges({
						value : switchButton.value
					});
				}
			});
		}
		switchButton.addEventListener('change', handleSwitchButtonChanges);
		if (osname === 'android') {
			switchButton.style = Titanium.UI.Android.SWITCH_STYLE_CHECKBOX;
		}
		var row = Ti.UI.createTableViewRow({
			itemId : text,
			height : '40dp',
			width : Ti.UI.FILL,
			className : 'leftDrawer'
		});

		var label = Ti.UI.createLabel({
			text : L(text),
			color : 'black',
			font : {
				fontSize : '14dp',
				fontWeight : 'bold'
			},
			itemId : text
		});
		label.left = (osname === 'iphone') ? '15dp' : '3dp';

		row.add(label);
		row.add(switchButton);

		return row;
	};
	// var newsSection = Ti.UI.createTableViewSection();
	// newsSection.headerTitle = L('news_title');

	// newsSection.add(createTableViewRow('breaking_news'));
	// newsSection.add(createTableViewRow('featured_news'));
	// newsSection.add(createTableViewRow('armenian'));
	// newsSection.add(createTableViewRow('lebanese'));
	// newsSection.add(createTableViewRow('global'));
	// newsSection.add(createTableViewRow('editorial'));
	// newsSection.add(createTableViewRow('sports'));
	// newsSection.add(createTableViewRow('cultural'));
	// newsSection.add(createTableViewRow('articles'));
	// newsSection.add(createTableViewRow('misc'));

	/**
	 * will create a view to apply it as header view to the table
	 */
	var tableViewSectionHeaderView = Ti.UI.createView({
		backgroundColor : 'black',
		height : '20dp'
	});
	var dateHeaderView = Ti.UI.createLabel({
		color : 'white',
		font : {
			fontSize : '13dp'
		},
		left : '10dp',
		text : L('settings_title')
	});
	tableViewSectionHeaderView.add(dateHeaderView);
	var settingsSection = Ti.UI.createTableViewSection({
		headerView : tableViewSectionHeaderView
	});

	settingsSection.add(createSwitchButton('push_notifications'));

	var table = Ti.UI.createTableView({
		data : [settingsSection],
		backgroundGradient : {
			type : 'linear',
			startPoint : {
				x : '0%',
				y : '50%'
			},
			endPoint : {
				x : '100%',
				y : '50%'
			},
			colors : [{
				color : '#e2dddb',
				offset : 0.0
			}, {
				color : '#ebe6e5',
				offset : 0.25
			}, {
				color : '#e2dddb',
				offset : 1.0
			}],
		}
	});

	return table;
};

// function createTableViewRow(text) {
// var row = Ti.UI.createTableViewRow({
// title : L(text),
// itemId : text,
// font : {
// fontSize : '14dp',
// fontWeight : 'bold'
// },
// left : '45dp',
// color : 'black',
// height : '40dp',
// width : Ti.UI.FILL,
// className : 'leftDrawer'
// });
// return row;
// };

module.exports = TableMenu;
