var NappDrawerModule = require('dk.napp.drawer');

function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView');
	var DetailView = require('ui/common/DetailView');
	var TableMenu = require('ui/common/TableMenu')();
	var services = require('services/rss');
	var categoryMapper = require('services/AztagDaily').categoryMapper;
	var currentCategory, detailView;

	/**
	 * function to be used by the refesh button
	 */
	function refresh(category) {
		centerLabel.setText(categoryMapper[category]);
		tableView.hide();
		activityIndicator.show();
		services.fetchFormApi(MasterView, category, addRowsToTableView);
	};

	/**
	 * callback that will add the views to the scroll views after they are fetched from the network
	 * @param {Object} views array of views that will be added to the scroll view
	 */
	function addRowsToTableView(rows) {
		activityIndicator.hide();
		tableView.setData(rows);
		tableView.show();
	};

	var activityIndicator = Ti.UI.createActivityIndicator({
		indicatorColor : '#ccc',
		style : Ti.UI.ActivityIndicatorStyle.BIG_DARK,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE
	});

	var leftMenuView = Ti.UI.createView({
		backgroundColor : 'white',
		width : Ti.UI.FILL,
		height : Ti.UI.FILL
	});

	var centerView = Ti.UI.createView({
		backgroundColor : '#e2dddb',
		width : Ti.UI.FILL,
		height : Ti.UI.FILL
	});

	// create a menu
	var leftTableView = TableMenu;
	leftMenuView.add(leftTableView);
	leftTableView.addEventListener("click", function(e) {
		if (e.row.itemId !== 'push_notifications') {
			currentCategory = e.row.itemId;
			drawer.toggleLeftWindow();
			centerLabel.setText(categoryMapper[e.row.itemId]);
			tableView.hide();
			activityIndicator.show();
			services.fetchFormApi(MasterView, e.row.itemId, addRowsToTableView);
		}
	});

	var actionBar = Ti.UI.createView({
		top : 0,
		height : "60dp",
		backgroundColor : "#fdc616"
	});
	var leftToolbarBtn = Ti.UI.createButton({
		backgroundImage : '/images/menu-black.png',
		left : '5dp',
		height : '25dp',
		width : '25dp'
	});
	leftToolbarBtn.addEventListener("click", function() {
		drawer.toggleLeftWindow();
	});
	var centerLabel = Ti.UI.createLabel({
		font : {
			fontSize : "24dp",
			fontWeight : "bold"
		},
		color : "black"
	});
	var refreshButton = Ti.UI.createButton({
		right : '6dp',
		backgroundImage : '/images/refresh-black.png',
		height : '25dp',
		width : '25dp'
	});
	refreshButton.addEventListener('click', function() {
		refresh(currentCategory);
	});
	actionBar.add(leftToolbarBtn);
	actionBar.add(centerLabel);
	actionBar.add(refreshButton);
	centerView.add(actionBar);

	// create interface
	var tableView = Ti.UI.createTableView({
		width : Ti.UI.FILL,
		top : '60dp',
		backgroundGradient : {
			type : 'linear',
			startPoint : {
				x : '0%',
				y : '50%'
			},
			endPoint : {
				x : '100%',
				y : '50%'
			},
			colors : [{
				color : '#ebe6e5',
				offset : 0.0
			}, {
				color : '#e2dddb',
				offset : 0.25
			}, {
				color : '#ebe6e5',
				offset : 1.0
			}],
		}
	});
	tableView.addEventListener('click', function(e) {
		/**
		 * this is to ensure that the item click is a news item not an error item
		 */
		if (e.row.currentItem) {
			detailView = DetailView(e.row.currentItem);
			detailView.open();
		}
	});
	centerView.add(activityIndicator);
	centerView.add(tableView);

	// CREATE THE MODULE
	var drawer = NappDrawerModule.createDrawer({
		fullscreen : false,
		leftWindow : leftMenuView,
		centerWindow : centerView,
		parallaxAmount : 0.2, //0-1
		shadowWidth : "40dp",
		leftDrawerWidth : "200dp",
		rightDrawerWidth : "200dp",
		animationMode : NappDrawerModule.ANIMATION_NONE,
		closeDrawerGestureMode : NappDrawerModule.CLOSE_MODE_MARGIN,
		openDrawerGestureMode : NappDrawerModule.OPEN_MODE_MARGIN,
		orientationModes : [Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT],
		navBarHidden : true,
		fullscreen : false,
	});

	/**
	 * the open event will be used for the intial refresh
	 */
	drawer.addEventListener('open', function(event) {
		activityIndicator.show();
		currentCategory = 'breaking_news';
		refresh(currentCategory);
		if (this.getActivity()) {
			var actionBar = this.getActivity().getActionBar();
			actionBar.hide();
		}
	});

	return drawer;
};

module.exports = ApplicationWindow;
