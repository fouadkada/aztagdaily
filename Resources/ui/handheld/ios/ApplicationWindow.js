var NappDrawerModule = require('dk.napp.drawer');

function ApplicationWindow() {
	//declare module dependencies
	var MasterView = require('ui/common/MasterView');
	var DetailView = require('ui/common/DetailView');
	var services = require('services/rss');
	var categoryMapper = require('services/AztagDaily').categoryMapper;
	var currentCategory, detailView;
	var drawer = NappDrawerModule.createDrawer();
	var TableMenu = require('ui/common/TableMenu')(drawer);
	TableMenu.addEventListener('click', function(e) {
		if (e.row.itemId !== 'push_notifications') {
			currentCategory = e.row.itemId;
			drawer.toggleLeftWindow();
			titleLabel.setText(categoryMapper[e.row.itemId]);
			// win.setTitle(categoryMapper[e.row.itemId]);
			tableView.hide();
			activityIndicator.show();
			services.fetchFormApi(MasterView, e.row.itemId, addRowsToTableView);
		}
	});

	/**
	 * function to be used by the refesh button
	 */
	function refresh(category) {
		titleLabel.setText(categoryMapper[category]);
		tableView.hide();
		activityIndicator.show();
		services.fetchFormApi(MasterView, category, addRowsToTableView);
	};

	/**
	 * callback that will add the views to the scroll views after they are fetched from the network
	 * @param {Object} views array of views that will be added to the scroll view
	 */
	function addRowsToTableView(rows) {
		activityIndicator.hide();
		tableView.show();
		tableView.setData(rows);
	};

	var titleLabel = Titanium.UI.createLabel({
		color : 'black',
		textAlign : 'center',
		font : {
			fontSize : '18dp',
			fontWeight : 'bold'
		}
	});

	var activityIndicator = Ti.UI.createActivityIndicator({
		indicatorColor : '#ccc',
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE,
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG
	});

	var leftBtn = Ti.UI.createButton({
		backgroundImage : 'images/menu-black.png',
		height : '20dp',
		width : '20dp'
	});
	leftBtn.addEventListener("click", function() {
		drawer.toggleLeftWindow();
	});
	var refreshBtn = Ti.UI.createButton({
		backgroundImage : 'images/refresh-black.png',
		height : '20dp',
		width : '20dp'
	});
	refreshBtn.addEventListener("click", function() {
		refresh(currentCategory);
	});

	var tableView = Ti.UI.createTableView({
		width : Ti.UI.FILL
	});
	tableView.addEventListener('click', function(e) {
		/**
		 * this is to ensure that the item click is a news item not an error item
		 */
		if (e.row.currentItem) {
			detailView = DetailView(e.row.currentItem);
			navController.openWindow(detailView, {
				animated : true
			});
		}
	});

	var win = Ti.UI.createWindow({
		barColor : "#fdc616",
		leftNavButton : leftBtn,
		rightNavButton : refreshBtn,
		backgroundColor : '#eee',
		translucent : false,
	});
	win.setTitleControl(titleLabel);
	win.add(tableView);
	win.add(activityIndicator);

	var navController = Ti.UI.iOS.createNavigationWindow({
		window : win
	});

	var leftWindow = Ti.UI.createWindow({
		top : '20dp'
	});
	leftWindow.add(TableMenu);

	drawer.setLeftWindow(leftWindow);
	drawer.setCenterWindow(navController);
	drawer.setCloseDrawerGestureMode(NappDrawerModule.CLOSE_MODE_PANNING_DRAWER);
	drawer.setOpenDrawerGestureMode(NappDrawerModule.OPEN_MODE_BEZEL_PANNING_CENTERWINDOW);
	//no shadow in iOS7
	drawer.setShowShadow(false);
	drawer.setLeftDrawerWidth(230);
	// remember to set UIViewControllerBasedStatusBarAppearance to false in tiapp.xml
	drawer.setStatusBarStyle(NappDrawerModule.STATUSBAR_WHITE);
	drawer.setOrientationModes([Ti.UI.PORTRAIT, Ti.UI.UPSIDE_PORTRAIT]);
	/**
	 * the open event will be used for the intial refresh
	 */
	drawer.addEventListener('open', function(event) {
		activityIndicator.show();
		currentCategory = 'breaking_news';
		refresh(currentCategory);
	});

	return drawer;
};

module.exports = ApplicationWindow;
