function ApplicationWindow() {
	var currentCategory, detailView;
	var services = require('services/rss');
	var categoryMapper = require('services/AztagDaily').categoryMapper;
	var TableMenu = require('ui/common/TableMenu')();
	var MasterView = require('ui/common/MasterView');
	var DetailView = require('ui/common/DetailView');
	var deviceTokenKey = 'deviceToken';
	TableMenu.backgroundColor = 'white';
	TableMenu.addEventListener('click', function(e) {
		if (e.row.itemId !== 'push_notifications') {
			currentCategory = e.row.itemId;
			titleLabel.setText(categoryMapper[e.row.itemId]);
			// win.setTitle(categoryMapper[e.row.itemId]);
			tableView.hide();
			activityIndicator.show();
			services.fetchFormApi(MasterView, e.row.itemId, addRowsToTableView);
		}
	});

	/**
	 * function to be used by the refesh button
	 */
	function refresh(category) {
		centerLabel.setText(categoryMapper[category]);
		tableView.hide();
		activityIndicator.show();
		services.fetchFormApi(MasterView, category, addRowsToTableView);
	};

	/**
	 * callback that will add the views to the scroll views after they are fetched from the network
	 * @param {Object} views array of views that will be added to the scroll view
	 */
	function addRowsToTableView(rows) {
		activityIndicator.hide();
		tableView.setData(rows);
		tableView.show();
	};

	//create object instance
	var self = Ti.UI.createWindow({
		backgroundColor : '#ffffff'
	});

	var actionBar = Ti.UI.createView({
		top : 0,
		height : "60dp",
		backgroundColor : "#fdc616"
	});

	var centerLabel = Ti.UI.createLabel({
		font : {
			fontSize : "24dp",
			fontWeight : "bold"
		},
		color : "black"
	});
	var refreshButton = Ti.UI.createButton({
		right : '6dp',
		backgroundImage : '/images/refresh-black.png',
		height : '50dp',
		width : '50dp'
	});
	refreshButton.addEventListener('click', function() {
		refresh(currentCategory);
	});
	actionBar.add(centerLabel);
	actionBar.add(refreshButton);

	var activityIndicator = Ti.UI.createActivityIndicator({
		indicatorColor : '#ccc',
		style : Ti.UI.ActivityIndicatorStyle.BIG_DARK,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE
	});

	//create table view container
	var tableContainer = Ti.UI.createView({
		top : 0,
		bottom : 0,
		left : 0,
		width : '30%'
	});
	tableContainer.add(TableMenu);
	self.add(tableContainer);

	var tableView = Ti.UI.createTableView({
		width : Ti.UI.FILL,
		top : '60dp',
		backgroundColor : '#FFF'
	});
	tableView.addEventListener('click', function(e) {
		/**
		 * this is to ensure that the item click is a news item not an error item
		 */
		if (e.row.currentItem) {
			detailView = DetailView(e.row.currentItem);
			detailView.open();
		}
	});

	//create detail view container
	var detailContainer = Ti.UI.createView({
		top : 0,
		bottom : 0,
		right : 0,
		left : '30%'
	});
	detailContainer.add(actionBar);
	detailContainer.add(activityIndicator);
	detailContainer.add(tableView);
	self.add(detailContainer);

	self.addEventListener('open', function() {
		activityIndicator.show();
		currentCategory = 'breaking_news';
		refresh(currentCategory);
	});

	return self;
};

module.exports = ApplicationWindow;
