var osname = Ti.Platform.osname;
module.exports = (osname === 'android') ? require('ui/handheld/android/ApplicationWindow') : require('ui/tablet/ipad/ApplicationWindow');
