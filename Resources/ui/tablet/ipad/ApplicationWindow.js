function ApplicationWindow() {
	var currentCategory;
	var detailNav;
	var services = require('services/rss');
	var categoryMapper = require('services/AztagDaily').categoryMapper;
	var TableMenu = require('ui/common/TableMenu')(detailNav);
	var MasterView = require('ui/common/MasterView');
	var DetailView = require('ui/common/DetailView');
	TableMenu.addEventListener('click', function(e) {
		if (e.row.itemId !== 'push_notifications') {
			currentCategory = e.row.itemId;
			detailWindow.setTitle(categoryMapper[e.row.itemId]);
			tableView.hide();
			activityIndicator.show();
			services.fetchFormApi(MasterView, e.row.itemId, addRowsToTableView);
		}
	});

	/**
	 * function to be used by the refesh button
	 */
	function refresh(category) {
		detailWindow.setTitle(categoryMapper[category]);
		tableView.hide();
		activityIndicator.show();
		services.fetchFormApi(MasterView, category, addRowsToTableView);
	};

	/**
	 * callback that will add the views to the scroll views after they are fetched from the network
	 * @param {Object} views array of views that will be added to the scroll view
	 */
	function addRowsToTableView(rows) {
		activityIndicator.hide();
		tableView.setData(rows);
		tableView.show();
	};

	/**
	 * create the window that will hold the table menu
	 */
	var tableWindow = Ti.UI.createWindow({
		backgroundColor : '#ffffff'
	});
	tableWindow.add(TableMenu);

	var tableNav = Ti.UI.iOS.createNavigationWindow({
		window : tableWindow
	});

	//create the window that will hold the news entries
	var detailWindow = Ti.UI.createWindow({
		backgroundColor : '#ffffff',
		title : 'TEST'
	});

	var activityIndicator = Ti.UI.createActivityIndicator({
		indicatorColor : '#ccc',
		style : Ti.UI.iPhone.ActivityIndicatorStyle.BIG,
		height : Ti.UI.SIZE,
		width : Ti.UI.SIZE
	});
	detailWindow.add(activityIndicator);

	var tableView = Ti.UI.createTableView({
		width : Ti.UI.FILL,
		backgroundColor : '#FFF'
	});
	tableView.addEventListener('click', function(e) {
		/**
		 * this is to ensure that the item click is a news item not an error item
		 */
		if (e.row.currentItem) {
			var detailView = DetailView(e.row.currentItem);
			detailNav.openWindow(detailView, {
				animated : true
			});
		}
	});
	detailWindow.add(tableView);

	detailNav = Ti.UI.iOS.createNavigationWindow({
		window : detailWindow
	});

	var refreshBtn = Ti.UI.createButton({
		backgroundImage : 'images/refresh-black.png',
		height : '40dp',
		width : '40dp'
	});
	refreshBtn.addEventListener("click", function() {
		refresh(currentCategory);
	});

	var splitWin = Ti.UI.iPad.createSplitWindow({
		detailView : detailNav,
		masterView : tableNav,
	});
	detailWindow.rightNavButton = refreshBtn;

	splitWin.addEventListener('visible', function(e) {
		if (e.view == 'detail') {
			e.button.title = L('settings_title');
			 // = Ti.UI.createButton({
				// backgroundImage : 'images/menu.png',
				// height : '40dp',
				// width : '40dp'
			// });
			detailWindow.leftNavButton = e.button;
		} else if (e.view == 'master') {
			detailWindow.leftNavButton = null;
		}
	});

	splitWin.addEventListener('open', function() {
		activityIndicator.show();
		currentCategory = 'breaking_news';
		refresh(currentCategory);
	});

	return splitWin;
}

module.exports = ApplicationWindow;
