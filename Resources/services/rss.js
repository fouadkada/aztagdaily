var apis = {
    featured_news : 'http://www.aztagdaily.com/archives/category/featured/feed',
    misc : 'http://www.aztagdaily.com/archives/category/10misc/feed',
    breaking_news : ['http://www.aztagdaily.com/feed?post_type=news&breaking_news=leb-mobile', 'http://www.aztagdaily.com/feed?post_type=news&breaking_news=arm-int-mobile'],
    cultural : 'http://www.aztagdaily.com/archives/category/05-cultural/feed',
    lebanese : 'http://www.aztagdaily.com/archives/category/03lebanese/feed',
    editorial : 'http://www.aztagdaily.com/archives/category/01-editorial/feed',
    armenian : 'http://www.aztagdaily.com/archives/category/02armenian/feed',
    sports : 'http://www.aztagdaily.com/archives/category/06sports/feed',
    global : 'http://www.aztagdaily.com/archives/category/04global/feed',
    articles : 'http://www.aztagdaily.com/archives/category/05-articles/feed'
};

exports.fetchFormApi = function(parent, itemId, cb) {
    if (Ti.Network.online) {
        var feeds = [];
        if (itemId === 'breaking_news') {
            var xhr = Titanium.Network.createHTTPClient();
            var apiURL = apis[itemId][0] + "&timestamp=" + new Date().getTime();
            xhr.open('GET', apiURL);
            xhr.onerror = function(e) {
                Ti.API.error('error fetching from API: ' + apiURL);
                Ti.API.error('error fetching from API: ' + ((e.error) || JSON.stringify(e)));
                if (parent.error) {
                    cb(parent.error());
                }
            };
            xhr.onload = function() {
                try {
                    feeds.push(this.responseText);
                    var xhr1 = Titanium.Network.createHTTPClient();
                    apiURL = apis[itemId][1] + "&timestamp=" + new Date().getTime();
                    xhr1.open('GET', apiURL);
                    xhr1.onerror = function(e) {
                        Ti.API.error('error fetching from API: ' + apiURL);
                        Ti.API.error('error fetching from API: ' + ((e.error) || JSON.stringify(e)));
                        if (parent.error) {
                            cb(parent.error());
                        }
                    };
                    xhr1.onload = function() {
                        try {
                            feeds.push(this.responseText);
                            if (parent.success) {
                                cb(parent.success(feeds));
                            }
                        } catch(e) {
                            Ti.API.debug('something went wrong, most probably there is no connection: ' + ((e.error) || JSON.stringify(e)));
                            cb(parent.error());
                        }
                    };
                    xhr1.send();
                } catch(e) {
                    Ti.API.debug('something went wrong, most probably there is no connection: ' + ((e.error) || JSON.stringify(e)));
                    cb(parent.error());
                }
            };
            xhr.send();
        } else {
            var xhr = Titanium.Network.createHTTPClient();
            var apiURL = apis[itemId] + "?timestamp=" + new Date().getTime();
            xhr.open('GET', apiURL);
            xhr.onerror = function(e) {
                Ti.API.error('error fetching from API: ' + apiURL);
                Ti.API.error('error fetching from API: ' + ((e.error) || JSON.stringify(e)));
                if (parent.error) {
                    cb(parent.error());
                }
            };
            xhr.onload = function() {
                try {
                    feeds.push(this.responseText);
                    if (parent.success) {
                        cb(parent.success(feeds));
                    }
                } catch(e) {
                    Ti.API.debug('something went wrong, most probably there is no connection: ' + ((e.error) || JSON.stringify(e)));
                    cb(parent.error());
                }
            };
            xhr.send();
        }
    } else {
        return cb(parent.offline());
    }
};
