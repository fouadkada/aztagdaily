module.exports = {
    categoryMapper : {
        featured_news : L('featured_news'),
        misc : L('misc'),
        breaking_news : L('breaking_news'),
        cultural : L('cultural'),
        lebanese : L('lebanese'),
        editorial : L('editorial'),
        armenian : L('armenian'),
        sports : L('sports'),
        global : L('global'),
        articles : L('articles')
    },
    fbAppId : '542855025819522'
};
